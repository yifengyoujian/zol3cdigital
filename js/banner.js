var num = 1;
var banner = true;
(function () {
    var kopy = $(".banner_imag_box>li");
    kopy.last().after(kopy.first().clone())
    kopy.first().before(kopy.last().clone())
})()
$(".banner_imag_box")[0].addEventListener("transitionend", function () {
    banner = true
})
function Banner() {
    num += 1
    $(".banner_imag_box").css({ left: `-${num * 1009}px`, transition: "all 800ms" })
    $(".banner_imag_butt > li").removeClass("on").eq(num - 1).addClass("on")
    banner = false
    num == 4 ? (function () {
        $(".banner_imag_butt > li").removeClass("on").eq(0).addClass("on")
        setTimeout(function () { $(".banner_imag_box").css({ transition: "all 0ms", left: `-1009px` }); banner = true; num = 1 }, 800)
    })() : null
}
function ba_first() {
    num -= 1
    $(".banner_imag_box").css({ left: `-${num * 1009}px`, transition: "all 800ms" })
    $(".banner_imag_butt > li").removeClass("on").eq(num - 1).addClass("on")
    banner = false
    num == 0 ? (function () {
        $(".banner_imag_butt > li").removeClass("on").eq(2).addClass("on")
        setTimeout(function () { $(".banner_imag_box").css({ transition: "all 0ms", left: `${-1009 * 3}px` }); banner = true; num = 3 }, 800)
    })() : null

}
$(".banner_imag_butt>li").click(function () {
    num = $(this).index()
    Banner()
})
$(".lef_rig_box>span").first().click(function () {
    banner ? ba_first() : null
})
$(".lef_rig_box>span").last().click(function () {
    banner ? Banner() : null
})
var time = setInterval(Banner, 2000)
$(".banner_imag").hover(function () {
    clearInterval(time)
}, function () {
    time = setInterval(Banner, 2000)
})